package no.ntnu.imt3281.util;

import java.awt.BorderLayout;
import java.awt.event.ItemEvent;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

public class ShowDerby extends JFrame {
	private static final String DBURL = "jdbc:derby:recordCollection";
	JComboBox<String> tables = new JComboBox<>();
	JLabel display = new JLabel();
	
	public ShowDerby () {
		super ("Derby database viewer");
        tables.addItemListener(e->{
        	if (e.getStateChange()==ItemEvent.SELECTED) {
        		StringBuffer html = new StringBuffer();
        		String table = (String) tables.getSelectedItem();
        		String sql = "SELECT * FROM "+table;
    			Connection con;
				try {
					con = DriverManager.getConnection(DBURL);
	        		Statement stmnt = con.createStatement(); 
	        		stmnt.executeQuery(sql);
	        		ResultSet rs = stmnt.getResultSet();
	        		ResultSetMetaData meta = rs.getMetaData();
	        		html.append("<html><body>\n<table border='1'>\n<thead>\n<tr>\n");
	        		for (int i=0; i<meta.getColumnCount(); i++) {
	        			html.append("<th>");
	        			html.append(meta.getColumnName(i+1));
	        			html.append("</th>\n");
	        		}
	        		html.append("</tr>\n</thead>\n<tbody>");
	        		while (rs.next()) {
	        			html.append("<tr>\n");
	        			for (int i=0; i<meta.getColumnCount(); i++) {
				        	html.append("<td>");
							html.append(rs.getString(i+1));
							html.append("</td>\n");
        				}
        				html.append("</tr>\n");
	        		}
	        		html.append("</tr>\n</tbody>\n</body></html>");
	        		display.setText(html.toString());
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
        	}
        });
        try {
			Connection con = DriverManager.getConnection(DBURL);
			DatabaseMetaData dbmd = con.getMetaData();
			ResultSet resultSet = dbmd.getTables(null, null, null, null);
			while (resultSet.next()) {
			    String strTableName = resultSet.getString("TABLE_NAME");
			    String strTableType = resultSet.getString("TABLE_TYPE");
			    if (strTableType.equals("TABLE")) {
			    	tables.addItem(strTableName);
			    }
			}
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		// Connect to database
        add(tables, BorderLayout.NORTH);
        add(new JScrollPane(display), BorderLayout.CENTER);
        setSize(800, 600);
        setVisible(true);
	}

	public static void main (String args[]) {
		new ShowDerby();
	}

}
