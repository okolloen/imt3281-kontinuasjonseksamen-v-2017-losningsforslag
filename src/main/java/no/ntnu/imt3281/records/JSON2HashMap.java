package no.ntnu.imt3281.records;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Class with one static method, used to convert a JSON data string into a hashmap.
 * @author oivindk
 *
 */
public class JSON2HashMap {
	/**
	 * Takes a String with JSON encoded data and convert it into a hashmap.
	 * 
	 * @param jSONData a string with JSON encoded data
	 * 
	 * @return a hashmap where the key is the full path of keys leading up to the value. 
	 * For arrays the key is an auto generated index. The value is the value for the given key
	 */
	public static HashMap<String, String> convert(String jSONData) {
		JSONParser parser = new JSONParser();
		try {
			JSONObject obj = (JSONObject) parser.parse(jSONData);
			HashMap<String, String> map = new HashMap<>();
			handleJSONObject(obj, "", map);
			return map;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Finds all items in a JSON object and inserts them into the given hashmap.
	 * 
	 * @param jsonObject the JSON object to get items from
	 * @param preKey a string to prepend to the key
	 * @param map the hashmap to add items to 
	 */
	private static void handleJSONObject (JSONObject jsonObject, String preKey, HashMap<String, String> map) {
		if (preKey.length()>0)
			preKey += ".";
		Set<String> keys = jsonObject.keySet();
    	for (String key : keys) {
    		Object o = jsonObject.get(key);
    		if (o instanceof JSONArray) {	// Key points to a JSON array
    			handleJSONArray((JSONArray)o, preKey+key, map);
    		} else if (o instanceof JSONObject) {	// Key points to a JSON object
    			handleJSONObject((JSONObject)o, preKey+key, map);
    		} else {	// Key points to a simple value
    			if (o!=null)	// Don't add null values
    				map.put(preKey+key, o.toString());
    		}
    	}
	}

	/**
	 * Finds all items in a JSON array and inserts them into the given hashmap. Use array index as key value.
	 * 
	 * @param arr the JSON array to get items from
	 * @param preKey a string to prepend to the key 
	 * @param map the hashmap to add items to 
	 */
	private static void handleJSONArray(JSONArray arr, String preKey, HashMap<String, String> map) {
		if (preKey.length()>0)
			preKey += ".";
		Iterator<String> iterator = arr.iterator();
		int idx = 0;
		while (iterator.hasNext()) {
    		Object o = iterator.next();
    		if (o instanceof JSONArray) {	// Key points to a JSON array
    			handleJSONArray((JSONArray)o, preKey+idx, map);
    		} else if (o instanceof JSONObject) {	// Key points to a JSON object
    			handleJSONObject((JSONObject)o, preKey+idx, map);
    		} else {	// Key points to a simple value
    			map.put(preKey+idx, o.toString());
    		}
    		idx++;
    	}
	}
}
