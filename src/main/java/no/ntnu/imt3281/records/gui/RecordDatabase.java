package no.ntnu.imt3281.records.gui;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import no.ntnu.imt3281.records.FrontCoverFinder;
import no.ntnu.imt3281.records.JSON2HashMap;
import no.ntnu.imt3281.records.Lyrics;
import no.ntnu.imt3281.records.ReadJSON;
import no.ntnu.imt3281.records.Release;
import no.ntnu.imt3281.records.Search;

public class RecordDatabase {
    @FXML private TextField artist;
    @FXML private TextField album;
    @FXML private ComboBox<Release> selectedRecord;
    @FXML private TableView<Track> showTracks;
    @FXML private TableColumn<Track, String> titleCol;
    @FXML private TableColumn<Track, String> lengthCol;
    @FXML private TableColumn<Track, String> artistCol;
    @FXML private TableColumn<Track, String> trackCol;
    @FXML private ImageView coverImage;
    @FXML private Label lyrics;
    private ObservableList<Track> ol_tracks = FXCollections.observableArrayList();
    
	private static final String DBURL = "jdbc:derby:recordCollection";
	
	// Code added for task 13
    @FXML private TableView<Release> myAlbums;
    @FXML private TableColumn<Release, String> myAlbumsArtist;
    @FXML private TableColumn<Release, String> myAlbumsTitle;
    @FXML private TableColumn<Release, String> myAlbumsMedia;
    @FXML private TableColumn<Release, String> myAlbumsDate;
    private ObservableList<Release> ol_albums = FXCollections.observableArrayList();

    /**
     * Added for task 13, will get all owned records from the database when
     * object is created.
     */
    public RecordDatabase() {
        try {
			Connection con = DriverManager.getConnection(DBURL);
	        String sql = "SELECT id, title, artist, media, country, releaseDate FROM record";
	        Statement stmnt = con.createStatement();
	        ResultSet res = stmnt.executeQuery(sql);
	        while (res.next()) {
	        	Release r = new Release();
	        	r.setId(res.getString(1));
	        	r.setTitle(res.getString(2));
	        	r.setArtist(res.getString(3));
	        	r.setMedia(res.getString(4));
	        	r.setCountry(res.getString(5));
	        	r.setDate(res.getString(6));
	        	ol_albums.add(r);
	        }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		// Connect to database
    }
    
    @FXML
    /*
     * This method is run when all components have been initialized.
     * Set up cellValueFactories for tables, also sets observableLists for said tables.
     */
    public void initialize() {
    	// Get value for title column from Track object, title field (through getter method)
    	titleCol.setCellValueFactory(new PropertyValueFactory<Track, String>("title"));
        lengthCol.setCellValueFactory(new PropertyValueFactory<Track, String>("length"));
        artistCol.setCellValueFactory(new PropertyValueFactory<Track, String>("artist"));
        trackCol.setCellValueFactory(new PropertyValueFactory<Track, String>("track"));
        showTracks.setItems(ol_tracks);	// Connect observableList to table view
        
        // Add listener to table that will get notified when selection changes (Task 9)
        showTracks.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Track>() {
            @Override
            public void changed(ObservableValue<? extends RecordDatabase.Track> value, Track oldValue, Track newValue) {
                //Check whether item is selected and set value of selected item to Label
                if(showTracks.getSelectionModel().getSelectedItem() != null) {
                	Track t = showTracks.getSelectionModel().getSelectedItem();	// Get selected track
                	String title = t.getTitle();								// Find title
                	String artist = t.getArtist();								// and artist
                	lyrics.setText(Lyrics.get (artist, title));					// Set lyric for song
                }
            }
        });
        
        // Code added for task 13
        myAlbumsArtist.setCellValueFactory(new PropertyValueFactory<>("artist"));
        myAlbumsTitle.setCellValueFactory(new PropertyValueFactory<>("title"));
        myAlbumsMedia.setCellValueFactory(new PropertyValueFactory<>("media"));
        myAlbumsDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        myAlbums.setItems(ol_albums);
    }
    
    @FXML 
    void searchArtistAlbum(ActionEvent event) {
    	HashMap<String, Release> releases = Search.search(album.getText(), artist.getText());
    	selectedRecord.setItems(FXCollections.observableArrayList(releases.values()));
    }

    @FXML
    void recordSelected(ActionEvent event) {
    	// separate non-FX thread, will not block GUI, image shows when ready
        new Thread() {

            // runnable for that thread
            public void run() {
            	String id = selectedRecord.getSelectionModel().getSelectedItem().getId();

            	// Information about tracks is found at this URL
            	String trackURL = "http://musicbrainz.org/ws/2/release/"+id+"?inc=artist-credits+labels+discids+recordings&fmt=json";
            	HashMap<String, String> tracks = JSON2HashMap.convert(
            										ReadJSON.read(trackURL));
            	int media = 0;
            	ol_tracks.clear();	// Clear observableList with tracks i.e. clear current list of tracks
            	while (tracks.containsKey("media."+media+".track-count")) {	// Loop over all media in current release
            		int offset = Integer.parseInt(tracks.get("media."+media+".track-offset"));
            		int count = Integer.parseInt(tracks.get("media."+media+".track-count"));
            		for (int track=offset; track<offset+count; track++) { 	// Loop over all tracks in this media
            			Track trackInfo = new Track();						// Create track and fill in information
            			trackInfo.setTrack (tracks.get("media."+media+".tracks."+track+".number"));
            			trackInfo.setLength (tracks.get("media."+media+".tracks."+track+".length"));
            			trackInfo.setTitle (tracks.get("media."+media+".tracks."+track+".title"));
            			trackInfo.setArtist (tracks.get("media."+media+".tracks."+track+".artist-credit.0.artist.name"));
                		ol_tracks.add(trackInfo);							// Add track to observableList and thus to the table
            		}
            		media++;
            	}
            	
            	
            	String url = FrontCoverFinder.getCoverURL(
            					JSON2HashMap.convert(	// Covers can be found at the URL below
            						ReadJSON.read("http://coverartarchive.org/release/"+id+"/")));
            	Image cover = new Image(url);
            	
                Platform.runLater(new Runnable() {	// Update GUI on GUI thread
	                public void run() {
                        coverImage.setImage(cover);
                    }
	            });
            }
        }.start();
        
    /*	
        // Simple solution, will block GUI
        String id = selectedRecord.getSelectionModel().getSelectedItem().getId();
    	String url = FrontCoverFinder.getCoverURL(
    					JSON2HashMap.convert(
    						ReadJSON.read("http://coverartarchive.org/release/"+id+"/")));
    	coverImage.setImage(new Image(url));*/
    }

    @FXML
    void addRecordToCollection(ActionEvent event) {
    	try {
            Connection con = DriverManager.getConnection(DBURL);		// Connect to database

            String sql = "INSERT INTO record (title, artist, media, country, releaseDate, cover) VALUES (?, ?, ?, ?, ?, ?)";
            PreparedStatement stmnt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);	// Prepare insert into record table
            Release record = selectedRecord.getSelectionModel().getSelectedItem();
            
            // ************** Added for task 13
            ol_albums.add(record);	// This works for now, but need to change id to id from DB to be able to connect to tracks 
            // ****************
            if (record==null)
            	return;
            
            stmnt.setString(1, record.getTitle());
            stmnt.setString(2, record.getArtist());
            stmnt.setString(3, record.getMedia());
            stmnt.setString(4, record.getCountry());
            stmnt.setString(5, record.getDate());
            URL url = new URL(FrontCoverFinder.getCoverURL(
					JSON2HashMap.convert(	// Covers can be found at the URL below
						ReadJSON.read("http://coverartarchive.org/release/"+record.getId()+"/"))));
            stmnt.setBinaryStream(6, url.openStream());
            stmnt.execute();
            ResultSet keys = stmnt.getGeneratedKeys();
            if (keys.next()) {
            	int id = keys.getInt(1);
            	sql = "INSERT INTO track (recordId, trackNr, title, artist, length) VALUES (?, ?, ?, ?, ?)";
            	stmnt = con.prepareStatement(sql);
        		stmnt.setInt(1, id);
            	for (int i=0; i<ol_tracks.size(); i++) {
            		Track t = ol_tracks.get(i);
            		stmnt.setString(2, t.getTrack());
            		stmnt.setString(3, t.getTitle());
            		stmnt.setString(4, t.getArtist());
            		stmnt.setString(5, t.getLength());
            		stmnt.execute();
            	}
            }
            con.close();
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
    
    /**
     * Each object of this class represents a row in the table showing tracks in 
     * albums that has been found through a search.
     * 
     * @author okolloen
     *
     */
    public class Track {

		private String length;
		private String title;
		private String artist;
		private String track;

		/**
		 * Set track number
		 * @param track
		 */
		public void setTrack(String track) {
			this.track=track;
		}

		/**
		 * Set artist name
		 * @param artist
		 */
		public void setArtist(String artist) {
			this.artist=artist;
		}

		/**
		 * Set title of track
		 * @param title
		 */
		public void setTitle(String title) {
			this.title=title;
		}

		/**
		 * Set length of track
		 * @param length
		 */
		public void setLength(String length) {
			this.length=length;
		}

		/**
		 * @return the length
		 */
		public String getLength() {
			return length;
		}

		/**
		 * @return the title
		 */
		public String getTitle() {
			return title;
		}

		/**
		 * @return the artist
		 */
		public String getArtist() {
			return artist;
		}

		/**
		 * @return the track
		 */
		public String getTrack() {
			return track;
		}
    	
    }

}