package no.ntnu.imt3281.records;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Class contains one static method that is used to read data from an URL.
 * 
 * @author oivindk
 *
 */
public class ReadJSON {

	/**
	 * Used to read data from an URL, alters the User-Agent to allow it to read from all sources.
	 * 
	 * @param strURL a string containing the URL to read data from 
	 * @return a string with the content of the given URL
	 */
	public static String read(String strURL) {
		URL url;
		try {
			url = new URL (strURL);
	    	URLConnection connection = url.openConnection();				// Need a bit extra
	    	// Must set user-agent, the java default user agent is denied
	    	connection.setRequestProperty("User-Agent", "curl/7.8 (i386-redhat-linux-gnu) libcurl 7.8 (OpenSSL 0.9.6b) (ipv6 enabled)");
	    	// Must set accept to application/json, if not html is returned
	    	connection.setRequestProperty("Accept", "application/json");
	    	connection.connect();

	    	// Read and parse the response
	    	BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	    	
	    	StringBuffer sb = new StringBuffer();
	    	String tmp = null;
	    	while ((tmp = br.readLine())!=null) 
	    		sb.append(tmp);
			return sb.toString();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
