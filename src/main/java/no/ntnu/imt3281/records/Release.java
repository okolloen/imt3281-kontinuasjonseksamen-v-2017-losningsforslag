package no.ntnu.imt3281.records;

/**
 * Objects of this class contains data about a record release
 * @author oivindk
 *
 */
public class Release {
	private String id;
	private String artist;
	private String title;
	private String media;
	private String country;
	private String date;
	
	public void setId(String id) {
		this.id = id;		
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setMedia(String media) {
		this.media = media;
	}

	public void setCountry(String country) {
		this.country = country;
		
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getArtist() {
		return artist;
	}

	public String getMedia() {
		return media;
	}

	public String getCountry() {
		return country;
	}

	public String getDate() {
		return date;
	}

	@Override
	public String toString() {
		return getArtist()+" : "+getTitle()+" - "+getMedia()+(getCountry()!=null?" ("+getCountry()+")":"");
	}
}
