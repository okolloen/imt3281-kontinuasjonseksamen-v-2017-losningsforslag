package no.ntnu.imt3281.records;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * Utility class with one static method used to get lyrics for a given song from http://lyrics.wikia.com/wiki/
 * 
 * @author oivindk
 *
 */
public class Lyrics {

	/**
	 * Get lyrics for a given song from a given artist.
	 * 
	 * @param artist the artist to find lyrics for
	 * @param title the name of the song to find lyrics for
	 * 
	 * @return a string with the lyrics for this song.
	 */
	public static String get(String artist, String title) {
		Document doc;
		try {
			doc = Jsoup.connect("http://lyrics.wikia.com/wiki/"+artist.replace(" ", "_")+":"+title.replace(" ", "_")).get();
			Element link = doc.select(".lyricbox").first();
			// Use html because it contains line breaks, then remove <br> tags and the div that sometimes comes along.
			return link.html().replaceAll("<br>", "").replaceAll("<div class=\"lyricsbreak\"></div>", "");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}
}
