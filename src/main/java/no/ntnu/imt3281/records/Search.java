package no.ntnu.imt3281.records;

import java.util.HashMap;

/**
 * Class with static method used to search for a release.
 * 
 * @author oivindk
 *
 */
public class Search {

	/**
	 * Method used to search for a release 
	 * @param albumName name of album to search for
	 * @param artist name of artist to search for
	 * 
	 * @return a hash map with the release id as key and the release as value
	 */
	public static HashMap<String, Release> search(String albumName, String artist) {
		HashMap<String, Release> releases = new HashMap<String, Release>();
		HashMap<String, String> map = new HashMap<>();
		String url = "http://musicbrainz.org/ws/2/release/?query="+albumName.replace(" ",  "+")+"%20AND%20artist:"+artist.replace(" ", "+")+"&fmt=json";
		map = JSON2HashMap.convert(ReadJSON.read(url));
		int idx=0;
		while (map.containsKey("releases."+idx+".id")) {
			Release release = new Release();
			release.setId(map.get("releases."+idx+".id"));
			release.setArtist(map.get("releases."+idx+".artist-credit.0.artist.name"));
			release.setTitle(map.get("releases."+idx+".title"));
			release.setMedia(map.get("releases."+idx+".media.0.format"));
			release.setCountry(map.get("releases."+idx+".country"));
			release.setDate(map.get("releases."+idx+".date"));
			releases.put(release.getId(), release);
			idx++;
		}
/*		map.entrySet().stream().forEach((entry)-> {
			System.out.println (entry.getKey()+"->"+entry.getValue());
		});*/
		return releases;
	}

}
