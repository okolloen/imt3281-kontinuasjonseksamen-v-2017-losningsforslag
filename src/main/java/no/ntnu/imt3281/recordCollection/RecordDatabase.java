package no.ntnu.imt3281.recordCollection;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * Main class for application, loads the fxml file and displays it as the application.
 * 
 * @author oivindk
 *
 */
public class RecordDatabase extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		AnchorPane root;
		try {
			Locale locale = Locale.getDefault();;
			ResourceBundle bundle = ResourceBundle.getBundle("i18n.I18N", locale);

			root = FXMLLoader.load(getClass().getResource("../records/gui/RecordDatabase.fxml"), bundle);
	        Scene scene = new Scene(root);
	        
	        primaryStage.setTitle("FXML Welcome");
	        primaryStage.setScene(scene);
	        primaryStage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}
