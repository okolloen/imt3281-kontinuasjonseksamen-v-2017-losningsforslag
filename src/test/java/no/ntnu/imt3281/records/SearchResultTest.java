/**
 * 
 */
package no.ntnu.imt3281.records;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

/**
 * @author oivindk
 *
 */
public class SearchResultTest {

	@Test
	public void testSearch() {
		/*
		 * Should fetch http://musicbrainz.org/ws/2/release/?query=sessions%20AND%20artist:bruce%20springsteen&fmt=json and
		 * parse the resulting json data to find relevant releases. The hashmap should have the release id as key and data 
		 * about the release in SearchResult. 
		 * The SearchResult class should contain the title of the release, the name of the artists, the country of the 
		 * release (if available) and the medium for the release (CD, Vinyl, etc). If more than one "medium" in "medium-list"
		 * return the first one.
		 * 
		 * Search.search takes two parameters, the first is the name (or part of the name) of the album to search for,
		 * the second parameter is the name (or part of the name) of the artist/band that recorded the album. 
		 */
		HashMap<String, Release> result = Search.search("sessions", "Bruce Springsteen");
		assertNotNull(result.get("adc319b4-ba9e-3866-8c28-2ffc1a13fe03"));			
		Release release = result.get("adc319b4-ba9e-3866-8c28-2ffc1a13fe03");
		assertEquals("adc319b4-ba9e-3866-8c28-2ffc1a13fe03", release.getId());		// Id of Release/Album is registered correctly
		assertEquals("We Shall Overcome: The Seeger Sessions", release.getTitle());	// Release/Album title
		assertEquals("Bruce Springsteen", release.getArtist());						// Release/Album artist
		assertEquals("CD", release.getMedia());										// Release/Album media (CD/DVD/Vinyl)
		assertEquals("GB", release.getCountry());									// Release/Album released in country
		assertEquals("2006-10-02", release.getDate());										// Release/Album date
		assertEquals("Bruce Springsteen : We Shall Overcome: The Seeger Sessions - CD (GB)", release.toString());
		
		release = result.get("4226faa6-610d-458c-b2d0-669caee932b7");
		assertEquals("Bruce Springsteen : The Essential Born to Run Sessions - CD", release.toString());
	}
}
